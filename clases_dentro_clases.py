class Animal:
    def __init__(self, p):
        self.peso = p

class Mamifero(Animal):
    def __init__(self, p, c):
        super().__init__(p) #super(del que viene "el padre") .init para que se construya Animal y p porque es lo que necesita para construirse
        self.crias = c

p1 = Animal(30)
p2 = Mamifero(25,8)