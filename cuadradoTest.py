from Ejercicio_clases_cuadrado_rectangulo import Rectangulo
import unittest

class RectanguloTestCase(unittest.TestCase):
    def test_perimetro(self):
        rectangulo01 = Rectangulo(7, 4)
        perimetro1 = rectangulo01.perimetro()
        self.assertEqual(perimetro1, 22)
    
    def test_area(self):
        rectangulo01 = Rectangulo(7, 4)
        area1 = rectangulo01.area()
        self.assertEqual(area1, 28)

unittest.main()