class Vehiculo:
    def __init__(self, m, v):
        self.matricula = m
        self.velocidad = v

class Coche(Vehiculo):
    def __init__(self, c, m, v):
        super().__init__(m, v)
        self.color = c