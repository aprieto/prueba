class Cuadrado:
    """Un ejemplo de una clase para los Cuadrados"""
    def __init__(self, l=1):
        self.lado = l
        self.miarea = self.lado ** 2
    
    def perimetro(self):
        return self.lado * 4

    def area(self):
        return self.miarea

class Rectangulo:
    """Un ejemplo de una clase para los cuadrados"""
    def __init__(self, b=1, h=2):
        self.base = b
        self.altura = h
    
    def perimetro(self):
        return self.base * 2 + self.altura * 2
    
    def area(self):
        return self.base * self.altura

cuadrado01 = Cuadrado(2)
rectangulo01 = Rectangulo(4, 7)

print("perimetro =", rectangulo01.perimetro())
print("area =", rectangulo01.area())
